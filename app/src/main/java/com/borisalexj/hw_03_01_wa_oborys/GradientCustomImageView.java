package com.borisalexj.hw_03_01_wa_oborys;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import static android.content.ContentValues.TAG;

/**
 * Created by user on 3/23/2017.
 */

public class GradientCustomImageView extends ImageView {

    public GradientCustomImageView(Context context) {
        this(context, null);
    }

    public GradientCustomImageView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public GradientCustomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, -1);
    }

    public GradientCustomImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        Log.d(TAG, "onDraw: ");

        RadialGradient gradient = new RadialGradient(200, 200, 200,
                new int[]{0x3F51B5FF, 0xFF408FF, 0x303F9FFF}, new float[]{(float) 0.33, (float) 0.50, (float) 0.75}, android.graphics.Shader.TileMode.REPEAT);

        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inScaled = true;
        opt.inMutable = true;

        Bitmap bitmap;

        try {
            bitmap = ((BitmapDrawable) getDrawable()).getBitmap();
        } catch (NullPointerException e) {
            return;
        }

        Paint paint = new Paint();
        paint.setShader(gradient);
        canvas.drawBitmap(bitmap, 000, 000, paint);
        canvas.drawRect(000, 000, canvas.getWidth(), canvas.getHeight(), paint);

    }
}
