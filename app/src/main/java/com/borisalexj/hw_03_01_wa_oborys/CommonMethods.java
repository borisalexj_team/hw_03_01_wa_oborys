package com.borisalexj.hw_03_01_wa_oborys;

import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * Created by user on 3/24/2017.
 */

public class CommonMethods {
    public static Rect getRectFromBitmap(Bitmap bitmap) {
        Rect rect = new Rect();
        rect.set(0, 0, bitmap.getWidth(), bitmap.getHeight());
        return rect;
    }

    public static Rect recalculateBitmapRect(Rect bitmapRect, Rect canvasRect) {
        Rect rect = new Rect();
        int xKoef = canvasRect.width() / bitmapRect.width();
        int yKoef = canvasRect.height() / bitmapRect.height();
        if (xKoef >= yKoef) {
            rect.set(0, (canvasRect.height() - bitmapRect.height()) / 2, bitmapRect.width() * yKoef, bitmapRect.height() * yKoef);
        } else if (yKoef > xKoef) {
            rect.set((canvasRect.width() - bitmapRect.width()) / 2, 0, bitmapRect.width() * xKoef, bitmapRect.height() * xKoef);
        }
        return rect;
    }
}
