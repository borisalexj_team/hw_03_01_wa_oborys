package com.borisalexj.hw_03_01_wa_oborys;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private ImageView originalImageView;
    private InvertCustomImageView invertImageView;
    private TrafaretCustomImageView trafaretImageView;
    private GradientCustomImageView gradientImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        originalImageView = (ImageView) findViewById(R.id.originalImageView);
        invertImageView = (InvertCustomImageView) findViewById(R.id.invertImageView);
        trafaretImageView = (TrafaretCustomImageView) findViewById(R.id.trafaretImageView);
        gradientImageView = (GradientCustomImageView) findViewById(R.id.gradientImageView);
    }

    public void invertGBButtonClick(View view) {
        invertImageView.setImageResource(R.mipmap.clip2357);
    }

    public void loadOriginalClick(View view) {
        originalImageView.setImageResource(R.mipmap.clip2357);
    }

    public void trafaretClick(View view) {
        trafaretImageView.setImageResource(R.mipmap.clip2357);
    }

    public void radialGradientClick(View view) {
        gradientImageView.setImageResource(R.mipmap.clip2357);
    }

}
