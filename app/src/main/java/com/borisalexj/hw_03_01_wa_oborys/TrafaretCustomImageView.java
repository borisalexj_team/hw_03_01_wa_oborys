package com.borisalexj.hw_03_01_wa_oborys;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Region;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import static android.content.ContentValues.TAG;

/**
 * Created by user on 3/23/2017.
 */

public class TrafaretCustomImageView extends ImageView {

    public TrafaretCustomImageView(Context context) {
        this(context, null);
    }

    public TrafaretCustomImageView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public TrafaretCustomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, -1);
    }

    public TrafaretCustomImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        Log.d(TAG, "onDraw: ");

        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inScaled = true;
        opt.inMutable = true;

        Bitmap bitmap;

        try {
            bitmap = ((BitmapDrawable) getDrawable()).getBitmap();
        } catch (NullPointerException e) {
            return;
        }

        Paint paint = new Paint();
        Path path = new Path();
        path.reset();
        int i = 15;
        path.moveTo(25, 10);
        path.lineTo(30 * i, 20 * i);
        path.lineTo(20 * i, 40 * i);
        path.close();

        canvas.clipPath(path, Region.Op.INTERSECT);
        canvas.drawBitmap(bitmap, 0, 0, paint);

    }
}
