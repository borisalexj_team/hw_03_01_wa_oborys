package com.borisalexj.hw_03_01_wa_oborys;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import static android.content.ContentValues.TAG;

/**
 * Created by user on 3/23/2017.
 */

public class InvertCustomImageView extends ImageView {

    public InvertCustomImageView(Context context) {
        this(context, null);
    }

    public InvertCustomImageView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public InvertCustomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, -1);
    }

    public InvertCustomImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.d(TAG, "onDraw: ");

        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inScaled = true;
        opt.inMutable = true;

        Bitmap bitmap;

        try {
            bitmap = ((BitmapDrawable) getDrawable()).getBitmap();
        } catch (NullPointerException e) {
            return;
        }

        float[] cmData = new float[]{
                1, 0, 0, 0, 0,
                0, -1, 0, 0, 255,
                0, 0, -1, 0, 255,
                0, 0, 0, 1, 0};

        Paint paint = new Paint();
        ColorMatrix colorMatrix = new ColorMatrix(cmData);
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));

        canvas.drawBitmap(bitmap, 0, 0, paint);

    }

}
